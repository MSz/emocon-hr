function [eventfiles] = extract_events(file_paths, event_dir, rep_old, rep_new)
%EXTRACT_EVENTS Extracts timings from pspm data file & saves event file.
%   assumptions for the experiment (SOA, markers) are hardcoded
%   file_paths : string array of files to process
%   event_dir: directory to put event files
%   rep_old, rep_new - arguments for replacement performed on file name

if ~ isstring(file_paths)
    warning('file_paths:invalid_input', 'Should be a string.\n'); return;
end

SOA = 8;
FL_R_MAX_TIME = SOA;
CS_MARK = [1 2];

eventfiles = strings(size(file_paths));

for n = 1:length(file_paths)

    data_path = file_paths(n);

    [~, ~, data] = pspm_load_data(data_path{1}, 'marker');

    of_interest = ismember(data{1,1}.markerinfo.value, CS_MARK);
    onsets = data{1,1}.data(of_interest);

    events = cell(1, 2);
    events{1} = [onsets onsets+FL_R_MAX_TIME]; % flex. resp. to CS
    events{2} = onsets + SOA; % fixed response to US

    % save in given folder under new name
    [~, name, ext] = fileparts(data_path);
    outname = fullfile(event_dir, strrep(name, rep_old, rep_new) + ext);   
    save(outname, 'events');
    eventfiles(n) = outname;
    
end

end

