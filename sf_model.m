model_dir = '/Volumes/MyBookPro/eksperyment_HR/pspm/models';
stats_dir = '/Volumes/MyBookPro/eksperyment_HR/pspm';
rest_pattern = '/Volumes/MyBookPro/eksperyment_HR/HR GSR/*/rest_pspm_*';  % /*/ for T1 & T2

rest_files = dir(rest_pattern);

data_files = strings(size(rest_files));
model_files = strings(size(rest_files));

for n = 1:length(rest_files)
    
    data_files(n) = fullfile(rest_files(n).folder, rest_files(n).name);
    model_files(n) = fullfile(model_dir, strrep(rest_files(n).name, 'pspm', 'model'));
    
end

% specify & estimate the model for SF

model.datafile = cellstr(data_files);
model.modelfile = cellstr(model_files);
model.timing = cell(size(data_files)); % empty because...
model.timeunits = 'whole';  % the timing is ignored

sf_options.overwrite = true;

if ~ isfolder(model_dir)
    mkdir(model_dir)
end

pspm_init
outfile = pspm_sf(model, sf_options);


%% Collect stats into a table

nrows = length(model_files);

mytab = table('Size', [nrows, 3], ...
    'VariableNames', ["subject", "condition", "ns_scr_freq"],...
    'VariableTypes', ["string", "string", "double"]);

expr = 'rest_model_(?<subject>[A-Z]{2})(?<condition>1|2|EXP1)';
mapping = containers.Map(["1", "2", "EXP1"], ["rest1", "rest2", "rest3"]);

for n = 1:nrows
    [filepath, name, ext] = fileparts(model_files(n));
    match = regexp(name, expr, 'names');
    
    mdl = load(model_files(n));
    
    mytab{n, 'subject'} = match.subject;
    mytab{n, 'condition'} = string(mapping(match.condition));
    mytab{n, 'ns_scr_freq'} = mdl.sf.stats;
    
end

writetable(mytab, fullfile(stats_dir, 'rest_stats.csv'));
