eda_dir = '/Volumes/MyBookPro/eksperyment_HR/HR GSR/1 tura';
event_dir = '/Volumes/MyBookPro/eksperyment_HR/pspm/events';

subjects = ["AG", "KF", "RP", "TJ"];

sessions = ["", "2", "3"];

% use matlab trickery to produce a list of file names
files = append("EXPHR", subjects, sessions', ".eeg"); % implicit expansion
files = reshape(files, [], 1); % 1 column
file_paths = cellstr(fullfile(eda_dir, files));

% set import parameters & options
import_spec = {
    struct('type', 'scr', 'channel', 1, 'transfer', 'none'), ...
    struct('type', 'marker')
    };
import_options = struct('overwrite', true);

% run the import
imported = pspm_import(file_paths, 'brainvision', import_spec, import_options);

% trim resting state
trim_options = struct('overwrite', true);
out = pspm_trim(imported, 0, 60, {'3', '3'}, trim_options);

% rename trimmed files
rest_paths = strings(length(out), 1);
for n = 1:length(out)
    oldpath = string(out{n});
    [filepath, name, ext] = fileparts(oldpath);
    newname = strrep(name, 'tpspm_EXPHR', 'rest_pspm_');
    
    % make suffixes consistent with 2nd round
    if endsWith(newname, '3')
        newname = strrep(newname, '3', 'EXP1');
    elseif not(endsWith(newname, '2'))
        newname = strcat(newname, '1');
    end
    
    newpath = fullfile(filepath, newname + ext);
    rest_paths(n) = newpath;
    movefile(oldpath, newpath);
end

% trim task: 30 s after rest end (i.e. after instructions) to file end
out = pspm_trim(imported, 60 + 30, 'none', {'3', '3'}, trim_options); % SPM trick - last marker must be present

task_paths = strings(length(out), 1);
for n = 1:length(out)
    oldpath = string(out{n});
    [filepath, name, ext] = fileparts(oldpath);
    newname = strrep(name, 'tpspm_EXPHR', 'task_pspm_');
    
    % make suffixes consistent with 2nd round
    if endsWith(newname, '3')
        newname = strrep(newname, '3', 'EXP1');
    elseif not(endsWith(newname, '2'))
        newname = strcat(newname, '1');
    end
    
    newpath = fullfile(filepath, newname + ext);
    task_paths(n) = newpath;
    movefile(oldpath, newpath);
end

% do additional trimming at the end for 4 files (spotted manually)
pspm_trim(fullfile(eda_dir, 'task_pspm_AG1.mat'), 0, 256, 'file', trim_options);
pspm_trim(fullfile(eda_dir, 'task_pspm_KF1.mat'), 0, 280, 'file', trim_options);
pspm_trim(fullfile(eda_dir, 'task_pspm_RP1.mat'), 0, 250, 'file', trim_options);
pspm_trim(fullfile(eda_dir, 'task_pspm_TJ2.mat'), 0, 280, 'file', trim_options);

movefile(fullfile(eda_dir, 'ttask_pspm_AG1.mat'), fullfile(eda_dir, 'task_pspm_AG1.mat'));
movefile(fullfile(eda_dir, 'ttask_pspm_KF1.mat'), fullfile(eda_dir, 'task_pspm_KF1.mat'));
movefile(fullfile(eda_dir, 'ttask_pspm_RP1.mat'), fullfile(eda_dir, 'task_pspm_RP1.mat'));
movefile(fullfile(eda_dir, 'ttask_pspm_TJ2.mat'), fullfile(eda_dir, 'task_pspm_TJ2.mat'));


% extract events from task files for pspm non-linear model
if ~ isfolder(event_dir)
    mkdir(event_dir)
end

eventfiles = extract_events(task_paths, event_dir, 'task_pspm', 'events');
