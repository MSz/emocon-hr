% Run PsPM DCM model estimation and export statistics for all subjects and
% sessions (task).

model_dir = '/Volumes/MyBookPro/eksperyment_HR/pspm/models';
event_dir = '/Volumes/MyBookPro/eksperyment_HR/pspm/events';
stats_dir = '/Volumes/MyBookPro/eksperyment_HR/pspm';
task_pattern = '/Volumes/MyBookPro/eksperyment_HR/HR GSR/*/task_pspm_*';  % /*/ for T1 & T2

task_files = dir(task_pattern);

data_files = strings(size(task_files));
event_files = strings(size(task_files));
model_files = strings(size(task_files));

for n = 1:length(task_files)
    
    data_files(n) = fullfile(task_files(n).folder, task_files(n).name);
    event_files(n) = fullfile(event_dir, strrep(task_files(n).name, 'task_pspm', 'events'));
    model_files(n) = fullfile(model_dir, strrep(task_files(n).name, 'pspm', 'model'));
    
end

% initialize pspm
pspm_init

% specify options (empty, since only defaults used)
options = struct();

% run model estimation 
% loop because pspm_dcm does not loop files internally
for n = 1:length(model_files)
    model.modelfile = model_files{n};
    model.datafile = data_files{n};
    model.timing = event_files{n};
    
    dcm = pspm_dcm(model, options);
end

% specify export options
exp_options.target = fullfile(stats_dir, 'task_stats_pspm.csv');
exp_options.statstype = 'param';
exp_options.delim = ',';

% export stats
model_files = cellstr(model_files);
pspm_exp(model_files, exp_options);

% pspm export does not include subject or condition
% make a list of subjects and conditions
expr = 'task_model_(?<subject>[A-Z]{2})(?<condition>1|2|EXP1)';
mapping = containers.Map(["1", "2", "EXP1"], ["ctr1", "ctr2", "exp"]);

subject = strings(size(model_files));
condition = strings(size(model_files));

for n = 1:length(model_files)
    [filepath, name, ext] = fileparts(model_files{n});
    match = regexp(name, expr, 'names');
    
    subject(n) = match.subject;
    condition(n) = mapping(match.condition);
end

% add columns to the stats table and save it
stats = readtable(exp_options.target);  % will alter column names
stats = addvars(stats, subject, condition, 'Before', 1);
stats = removevars(stats, width(stats));  % drop last column
writetable(stats, fullfile(stats_dir, 'task_stats.csv'));
